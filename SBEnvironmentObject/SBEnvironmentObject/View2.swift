//
//  View2.swift
//  SBEnvironmentObject
//
//  Created by Jose Antonio on 15/6/22.
//

import SwiftUI

struct View2: View {
    
    var body: some View {
        VStack {
            Text("View 2")
                .padding()
            View3()
        }
    }
}

struct View2_Previews: PreviewProvider {
    static var previews: some View {
        View2()
    }
}
