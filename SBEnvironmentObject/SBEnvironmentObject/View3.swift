//
//  View3.swift
//  SBEnvironmentObject
//
//  Created by Jose Antonio on 15/6/22.
//

import SwiftUI

struct View3: View {
    @EnvironmentObject var viewModel: ViewModel
    
    var body: some View {
        VStack {
            Text("View 3")
                .padding()
            Button("Incrementar") {
                viewModel.counter += 1
            }
        }
    }
}

struct View3_Previews: PreviewProvider {
    static var previews: some View {
        View3()
    }
}
