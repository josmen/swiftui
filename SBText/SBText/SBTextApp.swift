//
//  SBTextApp.swift
//  SBText
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

@main
struct SBTextApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
