//
//  ContentView.swift
//  SBText
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Text(Date(), style: .date)
            Text(Date(), style: .timer)
            Text(Date().addingTimeInterval(3600), style: .timer)
            Text(Date(), style: .time)
        }.padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
