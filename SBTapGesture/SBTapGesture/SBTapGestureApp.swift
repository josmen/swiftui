//
//  SBTapGestureApp.swift
//  SBTapGesture
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

@main
struct SBTapGestureApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
