//
//  ContentView.swift
//  SBStepper
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

struct ContentView: View {
    @State var iPhoneCounter: Int = 1
    var body: some View {
        Form {
            Stepper("iPhone \(iPhoneCounter)") {
                iPhoneCounter += 1
                print("[Incrementar] Número \(iPhoneCounter)")
            } onDecrement: {
                iPhoneCounter -= 1
                print("[Decrementar] Número \(iPhoneCounter)")
            }

        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
