//
//  SBStepperApp.swift
//  SBStepper
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

@main
struct SBStepperApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
