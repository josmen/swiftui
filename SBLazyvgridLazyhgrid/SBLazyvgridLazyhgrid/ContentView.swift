//
//  ContentView.swift
//  SBLazyvgridLazyhgrid
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

struct ContentView: View {
    let elements = 1...500
    let gridItems = [GridItem(.fixed(100)),
                     GridItem(.fixed(100)),
                     GridItem(.fixed(100))]
    
    var body: some View {
        ScrollView(.horizontal) {
            LazyHGrid(rows: gridItems) {
                ForEach(elements, id: \.self) { element in
                    VStack {
                        Circle()
                            .frame(height: 40)
                        Text("\(element)")
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
