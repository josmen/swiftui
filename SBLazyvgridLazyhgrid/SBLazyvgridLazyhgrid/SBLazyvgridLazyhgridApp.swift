//
//  SBLazyvgridLazyhgridApp.swift
//  SBLazyvgridLazyhgrid
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

@main
struct SBLazyvgridLazyhgridApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
