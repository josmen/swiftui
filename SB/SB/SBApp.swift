//
//  SBApp.swift
//  SB
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

@main
struct SBApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
