//
//  ContentView.swift
//  SB
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Image("youtube")
            .renderingMode(.template)
            .resizable()
            .scaledToFit()
            .frame(width: 96, height: 68)
            .foregroundColor(.green)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
