//
//  SBSceneStorageApp.swift
//  SBSceneStorage
//
//  Created by Jose Antonio on 15/6/22.
//

import SwiftUI

@main
struct SBSceneStorageApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
