//
//  ContentView.swift
//  SBSceneStorage
//
//  Created by Jose Antonio on 15/6/22.
//

import SwiftUI

struct ContentView: View {
    
    @SceneStorage("tweet") private var tweet: String = ""
    @SceneStorage("toogle_publish_best_hour") private var togglePublishBestHour: Bool = false
    
    var body: some View {
        Form {
            TextEditor(text: $tweet)
                .frame(width: 300, height: 300)
            Toggle("Publicar a la mejor hora", isOn: $togglePublishBestHour)
                .padding()
            HStack {
                Spacer()
                Button(togglePublishBestHour ? "Publicar a la mejor hora 🍀" : "Publicar ahora 🐥")
                {
                    print("Publicando...")
                }
                Spacer()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
