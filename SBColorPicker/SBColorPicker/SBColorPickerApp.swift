//
//  SBColorPickerApp.swift
//  SBColorPicker
//
//  Created by Jose Antonio Mendoza on 12/06/2022.
//

import SwiftUI

@main
struct SBColorPickerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
