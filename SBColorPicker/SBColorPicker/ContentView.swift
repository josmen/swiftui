//
//  ContentView.swift
//  SBColorPicker
//
//  Created by Jose Antonio Mendoza on 12/06/2022.
//

import SwiftUI

struct ContentView: View {
    @State var color: Color = .blue
    var body: some View {
        VStack {
            Rectangle()
                .foregroundColor(color)
                .frame(width: 300, height: 60)
            ColorPicker("Selecciona un color", selection: $color)
            Spacer()
        }.padding(60)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
