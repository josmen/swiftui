//
//  ContentView.swift
//  SBLabel
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Label("Suscribiros a SwiftBeta", systemImage: "hand.thumbsup.fill")
            .font(.largeTitle)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
