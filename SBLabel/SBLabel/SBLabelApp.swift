//
//  SBLabelApp.swift
//  SBLabel
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

@main
struct SBLabelApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
