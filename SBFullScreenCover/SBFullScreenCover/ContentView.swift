//
//  ContentView.swift
//  SBFullScreenCover
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

struct ContentView: View {
    @State var isPresented: Bool = false
    
    var body: some View {
        VStack {
            Text("View 1")
                .padding()
            Button("Ok") {
                isPresented = true
            }
        }
        .sheet(isPresented: $isPresented,
                         onDismiss: { isPresented = false },
                         content: {
                            ZStack {
                                Color.red.ignoresSafeArea()
                                Button("Bienvenido a SwiftBeta! 🎉") {
                                    isPresented = false
                                }
                            }
            })
         }
     }
                         
                         struct ContentView_Previews: PreviewProvider {
            static var previews: some View {
                ContentView()
            }
        }
