//
//  SBFullScreenCoverApp.swift
//  SBFullScreenCover
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

@main
struct SBFullScreenCoverApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
