//
//  ContentView.swift
//  SBSlider
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

struct ContentView: View {
    @State var iPhoneCounter: Float = 0.0
    @State var isEditing: Bool = false
    
    var body: some View {
        Form {
            Slider(value: $iPhoneCounter,
                   in: 0...10,
                   step: 1) {
                Text("Selecciona número de iPhones")
            } minimumValueLabel: {
                Text("min")
            } maximumValueLabel: {
                Text("max")
            } onEditingChanged: { (editing) in
                isEditing = editing
            }
            Text("\(iPhoneCounter)")
                .foregroundColor(isEditing ? .green : .black)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
