//
//  SBSliderApp.swift
//  SBSlider
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

@main
struct SBSliderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
