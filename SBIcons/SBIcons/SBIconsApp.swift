//
//  SBIconsApp.swift
//  SBIcons
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

@main
struct SBIconsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
