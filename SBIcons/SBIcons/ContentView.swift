//
//  ContentView.swift
//  SBIcons
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Image(systemName: "moon.circle.fill")
            .renderingMode(.original)
            .resizable()
            .scaledToFit()
            .frame(width: 200)
            .foregroundColor(.green)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
