//
//  ContentView.swift
//  SBSwipeActionsRefreshable
//
//  Created by Jose Antonio on 15/6/22.
//

import SwiftUI

struct Device {
    let name: String
    let systemImage: String
}

let arrayOfDevices = [
    Device(name: "iphone", systemImage: "iphone"),
    Device(name: "ipad", systemImage: "ipad"),
    Device(name: "pc", systemImage: "pc"),
    Device(name: "4k", systemImage: "4k.tv"),
    Device(name: "ipod", systemImage: "ipod"),
    Device(name: "laptop", systemImage: "laptopcomputer"),
]

struct ContentView: View {
    var body: some View {
        NavigationView {
            List {
                ForEach(arrayOfDevices, id: \.name) { device in
                    Label(device.name, systemImage: device.systemImage)
                        .swipeActions {
                            Button {
                                print("Favorito")
                            } label: {
                                Label("Favorito", systemImage: "star.fill")
                            }
                            .tint(.yellow)
                            Button {
                                print("Compartir")
                            } label: {
                                Label("Compartir", systemImage: "square.and.arrow.up")
                            }
                            .tint(.blue)
                        }
                        .swipeActions(edge: .leading) {
                            Button {
                                print("Borrar")
                            } label: {
                                Label("Borrar", systemImage: "trash.fill")
                            }
                            .tint(.red)
                        }
                }
            }
            .refreshable {
                print("Recargando...")
            }
            .navigationTitle("Devices")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
