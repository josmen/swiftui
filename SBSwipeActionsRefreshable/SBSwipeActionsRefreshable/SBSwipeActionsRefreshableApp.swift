//
//  SBSwipeActionsRefreshableApp.swift
//  SBSwipeActionsRefreshable
//
//  Created by Jose Antonio on 15/6/22.
//

import SwiftUI

@main
struct SBSwipeActionsRefreshableApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
