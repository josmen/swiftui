//
//  ContentView.swift
//  SBLink
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Link(destination: URL(string: UIApplication.openSettingsURLString)!, label: {
            Label("Settings", systemImage: "gear")
                .font(.title)
                .foregroundColor(.white)
                .padding()
                .background(Color.blue)
                .cornerRadius(12)
        })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
