//
//  SBLinkApp.swift
//  SBLink
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

@main
struct SBLinkApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
