//
//  ContentView.swift
//  SBToggle
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

struct ContentView: View {
    @State var isOn: Bool = false
    
    var body: some View {
        Form {
            Toggle("Suscríbete a SwiftBeta", isOn: $isOn)
            Text("\(isOn.description)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
