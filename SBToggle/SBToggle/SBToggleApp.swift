//
//  SBToggleApp.swift
//  SBToggle
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

@main
struct SBToggleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
