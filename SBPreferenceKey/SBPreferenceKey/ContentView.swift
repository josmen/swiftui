//
//  ContentView.swift
//  SBPreferenceKey
//
//  Created by Jose Antonio on 17/6/22.
//

import SwiftUI

struct CustomTitleKey: PreferenceKey {
    static var defaultValue: String = ""
    
    static func reduce(value: inout String, nextValue: () -> String) {
        if !value.isEmpty {
            return
        }
        value = nextValue()
    }
}

struct CustomNavigationView<Content: View>: View {
    @State private var title: String = "Navigation View"
    let content: Content
    
    init(@ViewBuilder content: () -> Content) {
        self.content = content()
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(title)
                .font(.largeTitle)
                .bold()
            GeometryReader { proxy in
                ScrollView {
                    content
                }
            }
        }
        .padding()
        .onPreferenceChange(CustomTitleKey.self) { value in
            print("Value \(value)")
            title = value
        }
    }
}

struct ContentView: View {
    var body: some View {
        CustomNavigationView {
            VStack {
                Text("Hello, world!")
                    .padding()
                    .customNavigationTitle(title: "SwiftBeta 1")
                Text("Hola, mundo!")
                    .padding()
                    .customNavigationTitle(title: "SwiftBeta 2")
            }
            
        }
    }
}

extension View {
    func customNavigationTitle(title: String) -> some View {
        modifier(CustomNavigationTitle(title: title))
    }
}

struct CustomNavigationTitle: ViewModifier {
    private var title: String
    
    init(title: String) {
        self.title = title
    }
    
    func body(content: Content) -> some View {
        content
            .preference(key: CustomTitleKey.self, value: title)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
