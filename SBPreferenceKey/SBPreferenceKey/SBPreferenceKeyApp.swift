//
//  SBPreferenceKeyApp.swift
//  SBPreferenceKey
//
//  Created by Jose Antonio on 17/6/22.
//

import SwiftUI

@main
struct SBPreferenceKeyApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
