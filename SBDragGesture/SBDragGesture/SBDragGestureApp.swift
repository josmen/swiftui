//
//  SBDragGestureApp.swift
//  SBDragGesture
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

@main
struct SBDragGestureApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
