//
//  SBTextfieldApp.swift
//  SBTextfield
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

@main
struct SBTextfieldApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
