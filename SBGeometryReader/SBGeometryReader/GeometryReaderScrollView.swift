//
//  GeometryReaderScrollView.swift
//  SBGeometryReader
//
//  Created by Jose Antonio on 16/6/22.
//

import SwiftUI

let arrayOfNames = [
    "Naiara",
    "Iago",
    "Zoe",
    "Itziar",
    "Maite",
    "Jose Antonio",
    "Iaiii",
    "Iaio",
    "Abuelo"
]

struct GeometryReaderScrollView: View {
    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack {
                ForEach(arrayOfNames, id: \.self) { name in
                    GeometryReader { proxy in
                        VStack {
                            Text("\(proxy.frame(in: .global).minY)")
                            Spacer()
                            Text("\(name)")
                                .frame(width: 370, height: 200)
                                .background(Color.green)
                                .cornerRadius(20)
                            Spacer()
                        }
                        .shadow(color: .gray, radius: 10, x: 0, y: 0)
                        .rotation3DEffect(Angle(degrees: Double(proxy.frame(in: .global).minY) - 47), axis: (x: 0.0, y: 10.0, z: 10.0)
                        )
                    }
                    .frame(width: 370, height: 300)
                }
            }
            .padding(.trailing)
        }
        .padding(.horizontal)
    }
}

struct GeometryReaderScrollView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReaderScrollView()
    }
}
