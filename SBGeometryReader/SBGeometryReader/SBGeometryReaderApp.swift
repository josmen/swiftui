//
//  SBGeometryReaderApp.swift
//  SBGeometryReader
//
//  Created by Jose Antonio on 16/6/22.
//

import SwiftUI

@main
struct SBGeometryReaderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
