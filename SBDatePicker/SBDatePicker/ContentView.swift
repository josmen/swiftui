//
//  ContentView.swift
//  SBDatePicker
//
//  Created by Jose Antonio Mendoza on 12/06/2022.
//

import SwiftUI

struct ContentView: View {
    @State var currentDate: Date = Date()
    
    var body: some View {
        Form {
            DatePicker("Fecha",
                       selection: $currentDate,
                       in: Date()...,
                       displayedComponents: .date
            )
            Text(currentDate, style: .date)
                .bold()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
