//
//  SBFormApp.swift
//  SBForm
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

@main
struct SBFormApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
