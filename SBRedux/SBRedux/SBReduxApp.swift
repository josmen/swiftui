//
//  SBReduxApp.swift
//  SBRedux
//
//  Created by Jose Antonio on 18/6/22.
//

import SwiftUI

@main
struct SBReduxApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
