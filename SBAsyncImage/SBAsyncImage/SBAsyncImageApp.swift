//
//  SBAsyncImageApp.swift
//  SBAsyncImage
//
//  Created by Jose Antonio on 15/6/22.
//

import SwiftUI

@main
struct SBAsyncImageApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
