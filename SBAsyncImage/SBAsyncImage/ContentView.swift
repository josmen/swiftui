//
//  ContentView.swift
//  SBAsyncImage
//
//  Created by Jose Antonio on 15/6/22.
//

import SwiftUI

struct ContentView: View {
    
    private let url = URL(string: "https://placebear.com/1000/1000")
    
    var body: some View {
        AsyncImage(url: url) { image in
            image
                .resizable()
                .scaledToFit()
                .cornerRadius(20)
                .padding()
        } placeholder: {
            ProgressView()
        }

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
