//
//  ContentView.swift
//  SBState
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

struct ContentView: View {
    @State var counter: Int = 0
    
    var body: some View {
        VStack {
            Text("Counter: \(counter)")
                .padding()
            Button("Incrementar Valor") {
                self.counter += 1
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
