//
//  SBStateApp.swift
//  SBState
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

@main
struct SBStateApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
