//
//  SBContextMenuApp.swift
//  SBContextMenu
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

@main
struct SBContextMenuApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
