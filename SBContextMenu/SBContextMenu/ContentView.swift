//
//  ContentView.swift
//  SBContextMenu
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
            .contextMenu(
                ContextMenu(menuItems: {
                    Button("SwiftUI") {
                        print("Quiero aprender SwiftUI")
                    }
                    Button("Xcode") {
                        print("Quiero aprender Xcode")
                    }
                    Button {
                        print("Quiero aprender Swift")
                    } label: {
                        Label("Swift", systemImage: "iphone")
                    }

                }))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
