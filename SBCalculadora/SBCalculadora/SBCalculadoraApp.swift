//
//  SBCalculadoraApp.swift
//  SBCalculadora
//
//  Created by Jose Antonio on 17/6/22.
//

import SwiftUI

@main
struct SBCalculadoraApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
