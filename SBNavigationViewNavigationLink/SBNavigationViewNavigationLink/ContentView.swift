//
//  ContentView.swift
//  SBNavigationViewNavigationLink
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            List {
                NavigationLink("Option Menu 1", destination: Text("Some information"))
                NavigationLink("Option Menu 2", destination: Button("Tap me!", action: {
                    print("Hello iOS Developer")
                }))
                Text("Option Menu 3")
                Text("Option Menu 4")
            }
            .navigationBarItems(trailing: Button("Done", action: {
                print("NavigationView Button")
            }))
            .navigationTitle("Menu")
            .navigationBarTitleDisplayMode(.large)
            .navigationBarHidden(false)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
