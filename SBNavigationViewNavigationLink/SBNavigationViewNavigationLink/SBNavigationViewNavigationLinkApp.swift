//
//  SBNavigationViewNavigationLinkApp.swift
//  SBNavigationViewNavigationLink
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

@main
struct SBNavigationViewNavigationLinkApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
