//
//  SBViewModifierApp.swift
//  SBViewModifier
//
//  Created by Jose Antonio on 17/6/22.
//

import SwiftUI

@main
struct SBViewModifierApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
