//
//  SBAppStorageApp.swift
//  SBAppStorage
//
//  Created by Jose Antonio on 15/6/22.
//

import SwiftUI

@main
struct SBAppStorageApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
