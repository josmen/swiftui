//
//  ContentView.swift
//  SBAppStorage
//
//  Created by Jose Antonio on 15/6/22.
//

import SwiftUI

struct ContentView: View {
    
    @State var name: String = ""
    @AppStorage("appStorageName") var appStorageName: String = ""
    
    var body: some View {
        Form {
            TextField("Username", text: $name)
            HStack {
                Spacer()
                Button("Guardar") {
                    appStorageName = name
                }
                .padding()
                Spacer()
            }
            HStack {
                Spacer()
                Button("Imprimir valor") {
                    if let text = UserDefaults.standard.string(forKey: "appStorageName") {
                        print(text)
                    }
                }
                .padding()
                Spacer()
            }
        }
        .onAppear {
            name = appStorageName
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
