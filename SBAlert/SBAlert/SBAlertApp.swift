//
//  SBAlertApp.swift
//  SBAlert
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

@main
struct SBAlertApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
