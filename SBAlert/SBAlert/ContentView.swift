//
//  ContentView.swift
//  SBAlert
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

struct ContentView: View {
    @State var isPresented: Bool = false
    var body: some View {
        VStack {
            Text("Suscríbete a SwiftBEta")
                .padding()
            Button("Aceptar") {
                isPresented = true
            }
            .alert(isPresented: $isPresented, content: {
                Alert(title: Text("Suscríbete"),
                      message: Text("Cada semana más"),
                      primaryButton: .default(Text("Aceptar"),
                                                          action: {
                    print("Button tapped")
                }),
                      secondaryButton: .destructive(Text("Cancelar")))
            })
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
