//
//  SBButtonApp.swift
//  SBButton
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

@main
struct SBButtonApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
