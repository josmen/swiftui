//
//  ContentView.swift
//  SBButton
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Button {
                print("Suscríbete a SwiftBeta")
            } label: {
                Text("Suscríbete".uppercased())
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.red)
                    .cornerRadius(8)
                    .shadow(color: .red, radius: 8, x: 4, y: 4)
            }
            Button {
                print("Dale Like al vídeo")
            } label: {
                Circle()
                    .fill(Color.blue)
                    .frame(width: 200, height: 200)
                    .shadow(radius: 10)
                    .overlay(Image(systemName: "hand.thumbsup.fill").foregroundColor(.white).font(.system(size: 70, weight: .bold)))
            }

            
        }

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
