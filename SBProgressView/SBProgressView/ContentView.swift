//
//  ContentView.swift
//  SBProgressView
//
//  Created by Jose Antonio Mendoza on 12/06/2022.
//

import SwiftUI

struct ContentView: View {
    @State var progress: Float = 0.0
    var body: some View {
        VStack {
            ProgressView(value: progress)
            Button("Cargando") {
                progress += 0.1
            }.padding(.top, 40)
        }.padding(.horizontal, 32)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
