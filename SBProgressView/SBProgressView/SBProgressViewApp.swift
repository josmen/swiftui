//
//  SBProgressViewApp.swift
//  SBProgressView
//
//  Created by Jose Antonio Mendoza on 12/06/2022.
//

import SwiftUI

@main
struct SBProgressViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
