//
//  SBListApp.swift
//  SBList
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

@main
struct SBListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
