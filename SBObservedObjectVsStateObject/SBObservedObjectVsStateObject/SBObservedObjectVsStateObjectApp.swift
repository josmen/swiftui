//
//  SBObservedObjectVsStateObjectApp.swift
//  SBObservedObjectVsStateObject
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

@main
struct SBObservedObjectVsStateObjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
