//
//  ContentView.swift
//  SBTextEditor
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

struct ContentView: View {
    @State var text: String = "Escribe algo..."
    @State var counter: Int = 0
    var body: some View {
        VStack {
            TextEditor(text: $text)
                .font(.title)
                .autocapitalization(.none)
                .disableAutocorrection(true)
                .foregroundColor(.blue)
                .padding()
                .onChange(of: text) { newValue in
                    counter = newValue.count
                }
            Text("\(counter)")
                .foregroundColor(counter <= 280 ? .green : .red)
                .font(.largeTitle)
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
