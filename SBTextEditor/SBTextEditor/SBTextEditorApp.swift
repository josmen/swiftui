//
//  SBTextEditorApp.swift
//  SBTextEditor
//
//  Created by Jose Antonio on 12/6/22.
//

import SwiftUI

@main
struct SBTextEditorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
