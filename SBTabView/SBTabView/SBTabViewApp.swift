//
//  SBTabViewApp.swift
//  SBTabView
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

@main
struct SBTabViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
