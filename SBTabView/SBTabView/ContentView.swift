//
//  ContentView.swift
//  SBTabView
//
//  Created by Jose Antonio on 13/6/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            RoundedRectangle(cornerRadius: 20)
                .padding()
                .foregroundColor(.blue)
            RoundedRectangle(cornerRadius: 20)
                .padding()
                .foregroundColor(.red)
            HomeView()
                .tabItem {
                    Image(systemName: "house.fill")
                    Text("Home")
                }
            ProfileView()
                .tabItem {
                    Image(systemName: "person.crop.circle.fill")
                    Text("Profile")
                }
        }
        .frame(height: 400)
        .tabViewStyle(PageTabViewStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct HomeView: View {
    var body: some View {
        VStack {
            Image(systemName: "house.fill")
                .resizable()
                .scaledToFit()
                .frame(height: 200)
            Text("Home")
                .padding(.top, 32)
        }
    }
}

struct ProfileView: View {
    var body: some View {
        Text("Profile")
            
    }
}
