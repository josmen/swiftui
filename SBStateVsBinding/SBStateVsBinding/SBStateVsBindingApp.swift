//
//  SBStateVsBindingApp.swift
//  SBStateVsBinding
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

@main
struct SBStateVsBindingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
