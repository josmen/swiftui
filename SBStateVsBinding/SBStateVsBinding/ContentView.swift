//
//  ContentView.swift
//  SBStateVsBinding
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

struct ContentView: View {
    @State var counter: Int = 0
    
    var body: some View {
        CounterView(counter: $counter)
    }
}

struct CounterView: View {
    @Binding var counter: Int
    
    var body: some View {
        VStack(spacing: 20) {
            Text("\(counter)")
                .font(.largeTitle)
                .padding()
            Button("Incrementar") {
                counter += 1
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
