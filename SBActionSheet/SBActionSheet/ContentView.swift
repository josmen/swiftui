//
//  ContentView.swift
//  SBActionSheet
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

struct ContentView: View {
    @State var isPresented: Bool = false
    
    var body: some View {
        VStack {
            Text("Hello, world!")
                .padding()
            Button("Aceptar") {
                isPresented = true
            }
        }
        .actionSheet(isPresented: $isPresented) {
            ActionSheet(title: Text("Aprende SwiftUI"),
                        message: Text("Elige la opción que quiera"),
                        buttons: [.default(Text("SwiftUI"),
                                           action: {
                                                print("Aprende con SwiftBeta")
                                            }),
                                  .default(Text("Xcode")),
                                  .destructive(Text("Cancelar"))
                        ])
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
