//
//  SBActionSheetApp.swift
//  SBActionSheet
//
//  Created by Jose Antonio on 14/6/22.
//

import SwiftUI

@main
struct SBActionSheetApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
